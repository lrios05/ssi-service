package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Category;
import com.dh.ssiservice.repositories.CategoryRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;

//@RunWith(MockitoJUnitRunner.class)
// esta declaracion es opcional para no definir dentro el setup MockitoAnnotations.initMocks(this);
public class CategoryServicesImplTest {
    private static final String OTRA_CAT = "OTRACAT";
    private List<Category> categorySet;
    private Category category = new Category();

    @Mock
    CategoryRepository categoryRepository;
    @InjectMocks
    CategoryServiceImpl categoryServicesImpl;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        categorySet = new ArrayList<>();
        //Category category = new Category();
        category.setName(OTRA_CAT);
        categorySet.add(category);
        when(categoryRepository.findAll()).thenReturn(categorySet);
    }

    @Test
    public void testGetCategories() {
        List<Category> result = categoryServicesImpl.findAll();
        //Assert.assertEquals(result, Arrays.<Category>asList(new Category()));
        //verify(categoryRepository,times(1)).findAll();
        verify(categoryRepository).findAll();
        assertEquals(result, categorySet); // para adecuar al mock
        assertEquals(result.get(0).getName(), OTRA_CAT);
    }

    @Test
    public void testFindByCode() {
        when(categoryRepository.findByCode(any())).thenReturn(Optional.of(categorySet));

        List<Category> result = categoryServicesImpl.findByCode("code");
        //assertEquals(result, Arrays.<Category>asList(new Category()));
        assertEquals(result, Collections.singletonList(category));
    }

    @Test
    public void testFindById() {
        //Category result = categoryServicesImpl.findById(Long.valueOf(1));
        //assertEquals(result, categorySet);
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.of(category));
        Category result = categoryServicesImpl.findById(1L);
        assertEquals(result, category);
    }

}
