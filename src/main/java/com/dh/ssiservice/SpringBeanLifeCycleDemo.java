package com.dh.ssiservice;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class SpringBeanLifeCycleDemo implements InitializingBean, DisposableBean, BeanNameAware, BeanFactoryAware, ApplicationContextAware {
    public SpringBeanLifeCycleDemo() {
        System.out.println("Constructor de SpringBeanLifeCycleDemo");
    }

    //postConstruct y preDestroy con
    @PostConstruct
    public void postConstruct() {
        System.out.println("inicia postConstruct");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("inicia preDestroy");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("setBeanFactory");
    }

    @Override
    public void setBeanName(String s) {
        System.out.println("setBeanName");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("destroy");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("afterPropertiesSet");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("setApplicationContext");
    }

    //beforeInit y afterInit
    public void beforeInit() {
        System.out.println("inicia beforeInit");
    }

    public void afterInit() {
        System.out.println("inicia afterInit");
    }
}
