package com.dh.ssiservice.bootstrap;

import com.dh.ssiservice.model.*;
import com.dh.ssiservice.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private CategoryRepository categoryRepository;
    private ContractRepository contractRepository;
    private SubCategoryRepository subCategoryRepository;
    private ItemRepository itemRepository;
    private EmployeeRepository employeeRepository;
    private PositionRepository positionRepository;

    public DevBootstrap(CategoryRepository categoryRepository, ContractRepository contractRepository, SubCategoryRepository subCategoryRepository, ItemRepository itemRepository, EmployeeRepository employeeRepository, PositionRepository positionRepository) {
        this.categoryRepository = categoryRepository;
        this.contractRepository = contractRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.itemRepository = itemRepository;
        this.employeeRepository = employeeRepository;
        this.positionRepository = positionRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        Category eppCategory = new Category();
        eppCategory.setCode("EPAA");
        eppCategory.setName("EPP");
        categoryRepository.save(eppCategory);

        // RES category
        Category resourceCategory = new Category();
        resourceCategory.setCode("RES");
        resourceCategory.setName("RESOURCE");
        categoryRepository.save(resourceCategory);

        // safety subcategory
        SubCategory safetySubCategory = new SubCategory();
        safetySubCategory.setCategory(eppCategory);
        safetySubCategory.setCode("SAF");
        safetySubCategory.setName("SAFETY");

        subCategoryRepository.save(safetySubCategory);

        // raw material subcategory
        SubCategory rawMaterialSubCategory = new SubCategory();
        rawMaterialSubCategory.setCategory(resourceCategory);
        rawMaterialSubCategory.setCode("RM");
        rawMaterialSubCategory.setName("RAW MATERIAL");

        subCategoryRepository.save(rawMaterialSubCategory);

        // Helmet Item
        Item helmet = new Item();
        helmet.setCode("HEL");
        helmet.setName("HELMET");
        helmet.setSubCategory(safetySubCategory);

        itemRepository.save(helmet);

        // ink Item
        Item ink = new Item();
        ink.setCode("INK");
        ink.setName("INK");
        ink.setSubCategory(rawMaterialSubCategory);
        itemRepository.save(ink);

        // John Employee
        Employee john = new Employee();
        john.setFirstName("John");
        john.setLastName("Doe");

        // Position
        Position position = new Position();
        position.setName("OPERATIVE");
        positionRepository.save(position);

        // contract
        Contract contract = new Contract();
        contract.setEmployee(john);
        contract.setInitDate(new Date(2010, 1, 1));
        contract.setPosition(position);

        john.getContracts().add(contract);
        employeeRepository.save(john);
        contractRepository.save(contract);

      /*  Employee lrios = new Employee();
        lrios.setFirstName("Omar");
        lrios.setLastName("Rios");

        // Position
        Position positionE = new Position();
        positionE.setName("EXECUTIVE");
        positionRepository.save(positionE);

        // contract
        Contract contract2 = new Contract();
        contract.setEmployee(lrios);
        contract.setInitDate(new Date(2018, 1, 1));
        contract.setPosition(positionE);

        lrios.getContracts().add(contract2);
        employeeRepository.save(lrios);
        contractRepository.save(contract2);*/
    }

}
