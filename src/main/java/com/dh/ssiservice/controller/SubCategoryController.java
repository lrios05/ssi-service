package com.dh.ssiservice.controller;

import com.dh.ssiservice.repositories.SubCategoryRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/subcategories")
public class SubCategoryController {
    private SubCategoryRepository subCategoryRepository;

    public SubCategoryController(SubCategoryRepository subCategoryRepository) {
        this.subCategoryRepository = subCategoryRepository;
    }

    @RequestMapping
    public String getAllSubCategories(Model model) {
        model.addAttribute("subcategories", subCategoryRepository.findAll());
        return "subcategories";
    }

    @RequestMapping("/{id}")
    public String getSubCategoryById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("subcategory", subCategoryRepository.findById(id).get());
        return "subcategory";
    }
}
